// ---------------------------------------------------------------------------
//  File name: PlaneBehavior.cpp
//  Project name: Project 3
// ---------------------------------------------------------------------------
//  Creator�s name and email: Daniel Morrow, morrowdt@goldmail.etsu.edu		
//  Course-Section: 002
//  Creation Date: 10/19/2015			
// ---------------------------------------------------------------------------
#include "PlaneBehavior.h"
#include "OGL3DObject.h"


PlaneBehavior::PlaneBehavior()
{
	this->state = REST;
	this->timer = 0;
}


PlaneBehavior::~PlaneBehavior()
{
}

/**
* Method Name: update <br>
* Method Purpose: Plane raises and lowers <br>
*
* <hr>
* Date created: 8/31/2015 <br>
*/
void PlaneBehavior::update(GameObject *object, float elapsedSeconds)
{
	OGL3DObject *theObject = (OGL3DObject*)object;
	this->timer += elapsedSeconds;

	if (this->state == MOVING_FORWARD)
	{
		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = ASCENDING;
		}
	}
	else if (this->state == ASCENDING)
	{
		float move = 4 * elapsedSeconds;
		theObject->referenceFrame.moveUp(move);

		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = IN_AIR;
		}
	}
	else if (this->state == IN_AIR)
	{
		if (this->timer >= 3)
		{
			this->timer = 0;
			this->state = DESCENDING;
		}
	}
	else if (this->state == DESCENDING)
	{
		float move = 4 * elapsedSeconds;
		theObject->referenceFrame.moveDown(move);

		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = ON_GROUND;
		}
	}
	else if (this->state == ON_GROUND)
	{
		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = REST;
		}
	}
	else
	{
		if (this->timer >= 2)
		{
			this->timer = 0;
			this->state = MOVING_FORWARD;
		}
	}

}
