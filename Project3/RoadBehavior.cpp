// ---------------------------------------------------------------------------
//  File name: Main.cpp
//  Project name: Project 3
// ---------------------------------------------------------------------------
//  Creator�s name and email: Daniel Morrow, morrowdt@goldmail.etsu.edu		
//  Course-Section: 002
//  Creation Date: 10/19/2015			
// ---------------------------------------------------------------------------
#include "RoadBehavior.h"
#include "OGL3DObject.h"


RoadBehavior::RoadBehavior()
{
	this->state = REST;
	this->timer = 0;
}


RoadBehavior::~RoadBehavior()
{
}

/**
* Method Name: update <br>
* Method Purpose: Road moves backwards to give illusion of plane flying forward <br>
*
* <hr>
* Date created: 8/31/2015 <br>
*/
void RoadBehavior::update(GameObject *object, float elapsedSeconds)
{
	OGL3DObject *theObject = (OGL3DObject*)object;
	this->timer += elapsedSeconds;

	if (this->state == MOVING_FORWARD)
	{
		float move = 100 * elapsedSeconds;
		theObject->referenceFrame.moveBackward(move);

		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = ASCENDING;
		}
	}
	else if (this->state == ASCENDING)
	{
		float move = 100 * elapsedSeconds;
		theObject->referenceFrame.moveBackward(move);

		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = IN_AIR;
		}
	}
	else if (this->state == IN_AIR)
	{
		if (this->timer >= 3)
		{
			glm::vec3 newPosition = theObject->referenceFrame.getPosition();
			newPosition[2] += 400; // Sets the road ahead of the plane and outside the view to make it look likes it comes in
			theObject->referenceFrame.setPosition(newPosition);
			this->timer = 0;
			this->state = DESCENDING;
		}
	}
	else if (this->state == DESCENDING)
	{
		float move = 100 * elapsedSeconds;
		theObject->referenceFrame.moveBackward(move);

		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = ON_GROUND;
		}
	}
	else if (this->state == ON_GROUND)
	{
		float move = 100 * elapsedSeconds;
		theObject->referenceFrame.moveBackward(move);
		if (this->timer >= 1)
		{
			this->timer = 0;
			this->state = REST;
		}
	}
	else
	{
		if (this->timer >= 2)
		{
			this->timer = 0;
			this->state = MOVING_FORWARD;
		}
	}
}
