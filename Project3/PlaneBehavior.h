#pragma once
#ifndef PLANE_BEHAVIOR
#define PLANE_BEHAVIOR

#include "IBehavior.h"

class PlaneBehavior :
	public IBehavior
{
public:
	enum State { REST, MOVING_FORWARD, ASCENDING, IN_AIR, DESCENDING, ON_GROUND };

protected:
	State state;
	float timer;

public:
	PlaneBehavior();
	virtual ~PlaneBehavior();

	void update(GameObject *object, float elapsedSeconds);
};

#endif