// ---------------------------------------------------------------------------
//  File name: Main.cpp
//  Project name: Project 3
// ---------------------------------------------------------------------------
//  Creator�s name and email: Daniel Morrow, morrowdt@goldmail.etsu.edu		
//  Course-Section: 002
//  Creation Date: 10/19/2015			
// ---------------------------------------------------------------------------
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glutilD.lib")

#include "GameEngine.h"
#include "WindowsConsoleLogger.h"
#include "GameWindow.h"
#include "OGLVertexShader.h"
#include "OGLFragmentShader.h"
#include "OGLShaderProgram.h"
#include "OGLShaderManager.h"
#include "TextFileReader.h"
#include "OGLGraphicsSystem.h"
#include "CoreSystem.h"
#include "GameWorld.h"
#include "GameObjectManager.h"
#include "OGLSphericalCamera.h"
#include "OGLViewingFrustum.h"
#include "CustomObjectLoader.h"
#include "PCInputSystem.h"
#include "WindowsTimer.h"

#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>
using std::string;
using namespace std;

/**
* Method Name: ReadConfig <br>
* Method Purpose: Reads the configuration for a windows from a file <br>
*
* <hr>
* Date created: 8/31/2015 <br>
*
* <hr>
*   @param  configFile - name of the configuration file
*/
void ReadConfig(string configFile, string &title, int &width, int &height, float &red, float& green, float &blue)
{
	string configData;		// storage for parsing config data
	ifstream fin;			// Input file stream
	fin.open(configFile);
	if (fin.fail())
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}

	while (!fin.eof())		// While there is data to be read...
	{
		getline(fin, configData); // Read in a line from the configuration file
		if (configData.find("title:") != string::npos)			// If the line is a title, set the title
			title = configData.substr(6, string::npos);
		if (configData.find("width:") != string::npos)			// If the line is the width, convert and set the width
			width = stoi(configData.substr(6, string::npos));
		if (configData.find("height:") != string::npos)			// If the line is the height, convert and set the height
			height = stoi(configData.substr(7, string::npos));
		if (configData.find("red:") != string::npos)			// If the line is the red value, convert and set the red value
			red = stof(configData.substr(4, string::npos));
		if (configData.find("green:") != string::npos)			// If the line is the green value, convert and set the green value
			green = stof(configData.substr(6, string::npos));
		if (configData.find("blue:") != string::npos)			// If the line is the blue value, convert and set the blue value
			blue = stof(configData.substr(5, string::npos));
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	string title;
	int width, height;
	float red, green, blue;

	ReadConfig("window.config", title, width, height, red, green, blue);

	wstring wString(title.begin(), title.end()); // Convert string to wstring

	GameEngine gameEngine(
		new WindowsConsoleLogger(),
		new CoreSystem(
		new TextFileReader()),
		new OGLGraphicsSystem(
		new OGLShaderManager(),
		new GameWorld(
		new GameObjectManager(),
		new OGLSphericalCamera()),
		new OGLViewingFrustum()),
		new GameWindow(
		wString,
		width,
		height,
		red,
		green,
		blue),
		new PCInputSystem(),
		new WindowsTimer()
		);

	OGLGraphicsSystem *graphics = (OGLGraphicsSystem*)gameEngine.getGraphicsSystem();
	TextFileReader *reader = gameEngine.getCoreSystem()->getTextFileReader();

	graphics->addVertexShader(
		"SimpleVertexShader",
		new OGLVertexShader());
	graphics->addVertexShader(
		"VertexShader3d",
		new OGLVertexShader(),
		reader->readContents("VertexShader3DPerspective.glsl"));
	graphics->addFragmentShader(
		"SimpleFragmentShader",
		new OGLFragmentShader());

	graphics->addShaderProgram(
		"SimpleShader",
		new OGLShaderProgram(),
		"SimpleVertexShader", "SimpleFragmentShader");
	graphics->addShaderProgram(
		"ShaderProgram3d",
		new OGLShaderProgram(),
		"VertexShader3d", "SimpleFragmentShader");

	gameEngine.initializeWindowAndGraphics();

	CustomObjectLoader loader;
	loader.loadObjects(graphics->getGameWorld()->getObjectManager());

	graphics->setActiveShaderProgram("ShaderProgram3d");
	graphics->setObjectsShaderProgram("ShaderProgram3d");

	OGLSphericalCamera *camera = (OGLSphericalCamera *)graphics->getGameWorld()->getCamera();
	camera->setPosition(25.0f, -30.0f, 30.0f);

	graphics->setUpViewingEnvironment();

	gameEngine.setupGame();
	gameEngine.run();

	return 0;
}
