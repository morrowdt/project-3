#pragma once
#ifndef ROAD_BEHAVIOR
#define ROAD_BEHAVIOR

#include "IBehavior.h"

class RoadBehavior :
	public IBehavior
{
public:
	enum State { REST, MOVING_FORWARD, ASCENDING, IN_AIR, DESCENDING, ON_GROUND};

protected:
	State state;
	float timer;

public:
	RoadBehavior();
	virtual ~RoadBehavior();

	void update(GameObject *object, float elapsedSeconds);
};

#endif