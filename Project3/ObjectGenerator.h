#pragma once
#ifndef OBJECT_GENERATOR
#define OBJECT_GENERATOR

#include "RGBA.h"

struct ElementArray {
	float *vertexData;
	short *indexData;
};

class ObjectGenerator
{
public:
	ObjectGenerator();
	~ObjectGenerator();

	static float* generateXZSurface(float width = 1, float depth = 1);
};

#endif

