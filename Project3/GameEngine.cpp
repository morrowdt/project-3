#include "GameEngine.h"
#include "Logger.h"
#include "OGLShaderManager.h"
#include "GameWindow.h"
#include "GraphicsSystem.h"
#include "CoreSystem.h"
#include "PCInputSystem.h"
#include "GameWorld.h"
#include "ITimer.h"
#include "GameObjectManager.h"
#include "OGLObject.h"
#include "OGLReferenceFrame.h"
#include "PlaneBehavior.h"
#include "RoadBehavior.h"

GameEngine::GameEngine(
	Logger *logger,
	CoreSystem *core, 
	GraphicsSystem *graphics, 
	GameWindow *window,
	PCInputSystem *inputSystem,
	ITimer *timer)
{
	this->logger = logger;

	this->core = core;
	this->core->setLogger(this->logger);

	this->graphics = graphics;
	this->graphics->setLogger(this->logger);

	this->window = window;
	this->window->setLogger(this->logger);
	this->window->setGameEngine(this);

	this->inputSystem = inputSystem;
	this->graphics->getGameWorld()->setInputSystem(this->inputSystem);

	this->timer = timer;

	this->initialized = false;
}

GameEngine::~GameEngine()
{
	delete this->logger;
	delete this->core;
	delete this->graphics;
	delete this->window;
	delete this->inputSystem;
	delete this->timer;
}

void GameEngine::initializeWindowAndGraphics()
{
	// First create the window
	this->initialized = this->window->create();
	if (this->initialized) {
		// Next, intialize the graphics system
		this->initialized = this->graphics->initialize();
	}
}

void GameEngine::setupGame()
{
	OGLObject* object = (OGLObject*)
		this->graphics->getGameWorld()->getObjectManager()->getObject("Plane");
	//object->referenceFrame.setPosition(glm::vec3(16, 2, 0));
	object->referenceFrame.setPosition(glm::vec3(0, 2, 0));
	object->setBehavior(new PlaneBehavior());

	object = (OGLObject*)
		this->graphics->getGameWorld()->getObjectManager()->getObject("Surface");
	//object->referenceFrame.setPosition(glm::vec3(16, 2, 0));
	object->referenceFrame.setPosition(glm::vec3(0, -0.01, 0));

	object = (OGLObject*)
		this->graphics->getGameWorld()->getObjectManager()->getObject("Airport");
	//object->referenceFrame.setPosition(glm::vec3(16, 2, 0));
	object->referenceFrame.setPosition(glm::vec3(-5, 2, -2));
	object->setBehavior(new RoadBehavior());

	for (int i = 0; i < 60; i++)
	{
		string name = "Road" + std::to_string(i + 1);
		OGLObject* object = (OGLObject*)
			this->graphics->getGameWorld()->getObjectManager()->getObject(name);
		//object->referenceFrame.setPosition(glm::vec3(16, 2, 0));
		object->referenceFrame.setPosition(glm::vec3(0, 0, i * 4 - 120));
		object->setBehavior(new RoadBehavior());
	}
}

void GameEngine::run()
{
	if (this->initialized){
		this->window->show();
		this->window->listenForEvents(this->timer);
	}
	else {
		if (this->logger) {
			this->logger->log("The engine was not initialized!");
		}
	}
}


