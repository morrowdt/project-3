// ---------------------------------------------------------------------------
//  File name: CustomObjectLoader.cpp
//  Project name: Project 3
// ---------------------------------------------------------------------------
//  Creator�s name and email: Daniel Morrow, morrowdt@goldmail.etsu.edu		
//  Course-Section: 002
//  Creation Date: 10/19/2015			
// ---------------------------------------------------------------------------
#include "CustomObjectLoader.h"
#include "OGL3DObject.h"
#include "ObjectGenerator.h"
#include "GameObjectManager.h"
#include "TextFileReader.h"

#include <gl\glew.h>
#include <cstdlib>
#include <ctime>

#include <Windows.h>
#include <iostream>
#include <fstream>
using namespace std;

CustomObjectLoader::CustomObjectLoader()
{
	srand((unsigned int)time(NULL));
}


CustomObjectLoader::~CustomObjectLoader()
{
}

void CustomObjectLoader::loadObjects(GameObjectManager *gameObjectManager)
{
	OGLObject *object;
	VBOObject* vboObject;

	object = new OGL3DObject("Surface"); // Create Surface
	object->setVertexData(ObjectGenerator::generateXZSurface(50, 80));
	vboObject = OGLObject::createVBOObject(
		"triangles", object->getVertexData(), GL_TRIANGLES);
	object->addVBOObject(vboObject);
	gameObjectManager->addObject("Surface", object);

	object = new OGL3DObject("Plane"); // Create Plane
	object->setVertexData(readObject("Plane.object"));
	vboObject = OGLObject::createVBOObject(
		"triangles", object->getVertexData(), GL_TRIANGLES);
	object->addVBOObject(vboObject);
	gameObjectManager->addObject("Plane", object);

	for (int i = 0; i < 60; i++) // Create 60 Roads
	{
		string name = "Road" + std::to_string(i + 1);
		object = new OGL3DObject(name);
		object->setVertexData(readObject("Road.object"));
		vboObject = OGLObject::createVBOObject(
			"triangles", object->getVertexData(), GL_TRIANGLES);
		object->addVBOObject(vboObject);
		gameObjectManager->addObject(name, object);
	}

	object = new OGL3DObject("Airport"); // Create Airport
	object->setVertexData(readObject("Airport.object"));
	vboObject = OGLObject::createVBOObject(
		"triangles", object->getVertexData(), GL_TRIANGLES);
	object->addVBOObject(vboObject);
	gameObjectManager->addObject("Airport", object);
}

/**
* Method Name: readObject <br>
* Method Purpose: Reads object data from a file <br>
*
* <hr>
* Date created: 8/31/2015 <br>
*
* <hr>
*   @param  configFile - name of the object file
*	@return float*
*/
float* CustomObjectLoader::readObject(string filename)
{
	string input;			// For temporarily storing data

	ifstream fin;			// Input file stream
	fin.open(filename);

	if (fin.fail())
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}

	getline(fin, input); // First line is number of points

	int points = stoi(input);

	float* data = new float[points + 1]; // Create data based on number of points
	data[0] = points;					 // First data is number of points

	int i = 1;							 // Indexer

	while (!fin.eof())
	{
		getline(fin, input);			 // Read data
		data[i++] = stof(input);		 // Convert to float
	}

	return data;
}