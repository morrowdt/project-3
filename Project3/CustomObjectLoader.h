#pragma once
#ifndef CUSTOM_OBJECT_LOADER
#define CUSTOM_OBJECT_LOADER

#include "ObjectLoader.h"
#include <string>
using namespace std;

class CustomObjectLoader : public ObjectLoader
{
public:
	CustomObjectLoader();
	~CustomObjectLoader();

	void loadObjects(GameObjectManager *gameObjectManager);
	float* readObject(string filename);
};

#endif
